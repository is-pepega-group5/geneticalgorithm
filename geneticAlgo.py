'''

Genetic Algorithm for "Nurse Scheduling Problem"
Author:
- Muhammad Rafadana Mountheira
- Rayhan Muzakki
- Priagung Satria W 

Gitlab: https://gitlab.com/is-pepega-group5/geneticalgorithm

'''
import xlrd
import random
import math
import csv
import xlsxwriter
'''
FOR TESTING PURPOSE
    JUMLAH HOSPITAL = 10
    POPLATION SIZE = 10
    SAMPAI GENERASI KE 10
'''
#class data, contains data for patients, hospital, and nurses
TOURNAMENT_SIZE = 3
POPULATION_SIZE = 50
class Data:
    def __init__(self,patientData):
        #list of nurses
        self.nurseData = [ "Kristin Mellsop", "Gav Arbor", "Cchaddie De Cleen", "Montague Twaits", "Susanne Beet", "Gina Springer",
        "Stoddard Splevings", "Marta Babington", "Anastasia Muggleston", "Amandi Old", "Riordan Mattson", "Ines Kearle", "Fanechka Regi",
        "Didi Carlens", "Edee McLae", "Corinna Woliter", "Johann Castellani", "Emlyn Gligorijevic", "Pauletta Outram", "Kaye Cund",
        "Danny Letch", "Averyl Jennens", "Karlan Stalf", "Ortensia Baggaley", "Tamma Munn", "Shaylah Frere", "Beverley Verduin", "Dode Darling", "Alyss Birtle", "Granger Currier", "Dorella Chittie", "Tiebold Jessope", "Amalie Trodler", "Broddie Couvet", "Timofei Bothams", "Karalee Vasilenko", "Leigha Kinnerk", "Cindie Sturmey", "Kit Gwillyam", "Ian Saggers", "Jarad Winfred", "Putnem Holdin", "Harrietta Josephov", "Lovell Landsborough", "Parker Hadenton", "Edythe Speak", "Charmane Kirman", "Lancelot Braime", "Fran Rojahn", "Zabrina Kleinpeltz", "Jarrod Hasty", "Nichole Tafani", "Damien O' Molan", "Betti Pickvance", "Shelley Greville", "Gregorius Drennan", "Abagael Swindle", "Adey Dowd", "Tab Hounsom", "Fiorenze Churchill", "Egor Pleaden", "Orel Georgeou", "Leticia Cargo", "Marrilee Tall", "Thornie Aspling", "Millisent Hauger", "Tymothy Hollyland", "Saunderson Neill", "Daven Oehme", "Mary Wauchope", "Meggy Crole", "Angy Twelvetree", "Anton Belliard", "Melisenda Benezet", "Nicolette Teal", "Adelina Gove", "Ailyn Spurier", "Dana Klimochkin", "Maryjane Kemell", "Alma Henrichs", "Sanford Hacker", "Rebeca Naisby", "Odessa Chinnery", "Rey Feldstern", "Danielle Skerratt", "Bel Christall", "Artair Linkie", "Emylee Daulby", "Rad Eisak", "Lizette Hastewell", "Kaylee Robben", "Florina Mayger", "Olimpia Adamoli", "Luz Geekie", "Kippy Von Gladbach", "Derron Coveley", "Xavier Robroe", "Nancee Kalberer", "Bank Courtese", "Burgess Pozzo", "Palm Freake", "Edwina Beves", "Yasmeen Labba", "Dreddy M'Quharg", "Forester Haisell", "Arie Ratke", "Bebe Fullbrook", "Rodney Spaunton", "Malachi Jorger", "Brynna Torri", "Arne Cordeux", "Zorina McGreay", "Fletch Hazeldine", "Patricia Haywood", "Hobie Canti", "Arlette Edgler", "Eustace Huburn", "Caro Monshall", "Graeme Shelper", "Gaylor Sowray", "Kory Murty", "Garry Frossell", "Diahann Sloss", "Martelle Queyos", "Carson Ibbitson", "Leodora Donativo", "Raymond Gallamore", "Marlie Bertome", "Sisile Hurkett", "Harmony Madill", "Kerby Jurs", "Cameron Dibdale", "Lorens Tarply", "Amye Duer", "Stacy Niese", "Glynda Christophle", "Dar Adamsen", "Shaine Kahan", "Freemon Shevels", "Damaris Semiraz", "Rube Ranaghan", "Carmina Filchagin", "Cherianne Goldhill", "Benedict Ennion", "Randal Vanichkov", "Latrina Pendered", "Jilli Dovinson", "Chelsea Mouland", "Rosalie Rowbotham", "Rance Hanse", "Alvinia Schneider", "Leroi Shakesby", "Camile Barwise", "Ancell Pinch", "Marlee Kopke", "Alvy Hawkin", "Cheston Piggen", "Cart Kissack", "Godart Cuckoo", "Morgana Gyurkovics", "Leicester Norrie", "Corrinne Whitham", "Noe Shellcross", "Jeannine Windus", "Chloe Tremontana", "Aggy Tume", "Loraine Gonsalo", "Haily Sparshott", "Jeremy Mingay", "Richart Plowright", "Reube Ricks", "Silvano Woolbrook", "Bonny Sarge", "Kalle Bencher", "Fredra Kinane", "Weider Croney", "Neville Sperrett", "Clayborn Enrico", "Brenn Blackey", "Erastus Connelly", "Dniren Twelvetrees", "Brinn Haire", "Bettine Leyrroyd", "Fin Rulten", "Hodge Covert", "Dorothy Frichley", "Valaria Dregan", "Curtice Gartell", "Mariquilla Magenny", "Lorne Raveau", "Milly Colleran", "Pascale Wiggam", "Britte Blake", "Gail Aubert", "Forster Lerohan", "Abbott Rash", "Clareta Feaveer", "Robinia Usherwood", "Malanie Filoniere", "Syd Rosenwald", "Fara Lared", "Addy Dunphie", "Andris Bettinson", "Nady Grieg", "Gregor Witling", "Shandee Gallop", "Marris Friedman", "Poul Songhurst", "Abner Bifield", "Roobbie Imm", "Hallsy Coomes", "Tibold Macellar", "Karlen Chapellow", "Chris Schuricke", "Danyelle Hayto", "Carlotta Beadles", "Selia Bente", "Griswold Stemson", "Giff Vaudre", 
        "Marj Brosenius", "D'arcy Raw", "Pauli MacMakin", "Dawna Evison", "Remus Snowball", "Dell Lujan", "Kelsi Treend", "Cybill Stiddard",
        "Padget Dolder", "Raquel Bathowe", "Aline Matyugin", "Guthrie Bogey", "Idelle Tamburo", "Candide Stonbridge", "Aluin Huckell", "Bette Soares", "Cristine Grimwad", "Sibelle Chaytor", "Dominique Waldie", "Alano Wasiel", "Tanney Bertson", "Ceil Tonbridge", "Cherish Jorio", "Fancie Daniau", "Shayne Elam", 
        "Kendell Landa", "Brendin Ruberry", "Wadsworth Gotcher", "Ailee Trencher", "Tish Dumphrey", "Amble Poltone", "Otes Voyce", 
        "Vilhelmina Archbould", "Walliw Lorand", "Marika Gerred", "Sharia Gashion", "Jenni Wippermann", "Kimbell Wickins", 
        "Waverley Arrowsmith", "Flossi Boyton", "Lyn Dukelow", "Gay Roycroft", "Nerty Pietrowski", "Rachelle Connock", "Fulvia Knibley", 
        "Nicoli Huett", "Alison Hursey", "Vincents Adamovsky", "Magdalena Burnet", "Tanner Ullrich", "Vallie Tye", "Stephi Jeratt", 
        "Merill De Mitri", "Franny Lodevick", "Yuma Roswarne", "Joelie Hriinchenko", "Harold Cotes", "Babita Culwen", "Leah Blankman", 
        "Christy Jewitt", "Felicdad Lowndsbrough", "Ardene Cowlishaw", "Hedvige Sumbler", "Cammy Woodington", "Alvera Sanz", 
        "Bertie Horburgh", "Moise Gaiter", "Lynett Harbison", "Vern Line", "Darill Ziems", "Lindy Letertre", "Alexandra Seeler", 
        "Adam Fenn", "Hammad Dearlove", "Ralina Madison", "Daven Pinchon", "Alexandre Stobart", "Saleem Fieldstone", "Enos O'Brogan", 
        "Fitzgerald Siddeley","Jenni Goseling","Christian Pero","Gwyn Peasegood","Jessa Jamblin","Kissiah McDarmid","Denni Glander","Maury Kobiera",
        "Rodolph Kelf","Carmella Moulden","Shell Lacrouts", "Ivy Joules","Erhart Batchellor","Raul Benettini","August Podd","Adamo Jerrold","Fredric Esmonde","Pippy Tweedie",
        "Randolph Dowdam","Magdalen Jeens","Cathlene Stow","Rolland Smuth","Jermayne Moon","Netty Halls","Eba Giffin","Jacqueline Giraudat","Emilia Pelling",
        "Brooks Yanshonok","Linc Yosevitz","Amos Tender","Courtnay Binns","Gib Sawyer","Latia Lorkin","Carmon Barrand",'Witty Hedgecock','Eberhard Brokenshaw',
        'Holden Trahair','Rudolfo Liveing','Ax Sanbroke','Malinde Matzeitis','Eli Rockall','Malanie Cussins','Alejoa Ogg','Chloe Reedman','Charity Crippin',
        'Mellisa Chrippes','Blithe Fleming','Bobby Rassell','Karole Ryburn','Jori Windebank','Arleta Sherewood','Letty Turbat','Michele Dymick',
        'Kathleen Raffels','Lars Stoffels','Kari Abelson','Fianna Lempel','Linnea Vevers','Austen Lowcock','Giff Pecha','Alfreda Vesty','Idette Blaby',
        'Katha Otteridge','Job Goldsworthy','Perl Stockdale','Elberta Auchterlony','Bobby Farrent','Marja Dog','Nevin Thing','Jada Hackleton','Milissent Inskipp','Arty Wellbeloved',
        'Naomi Ashard','Aurlie Stembridge','Saree Rennebeck','Netti Plumbe','Willi Ovill','Jacintha Carletto','Olenolin Swigg','Beryl Zoellner',
        'Lyn Oherlihy','Sybil Dullard','Tamma Bebbell','Aeriela Gather','Carmine Dukesbury','Celestyna Peetermann','Carmel Hexham','Witty McTerry',
        'Isadora Asals','Yvon Middle','Luci Scardifeild','Lindsay Hove','Isabella Shemmin','Andrew Stollenhof','Arlan Jeenes','Hobie Tattershaw','Demetre Gabriel',
        'Jolyn Culross','Marty Jann','Judith Suche','Ty Kinghorn','Adelheid Wareing','Towny Curuclis','Alaine Bodicum','Melissa Doe','Arvy Lumpkin',
        'Lotty Maginn','Uri Seres','Michal Ickowics','Melloney OHickee','Idelle Geekie','Trixie Heald','Tracie Blackhurst','Lorette Raffin','Amerigo Halstead',
        'Austin Melly','Elroy Wallege','Anissa Jellis','Goddard Cuel','Reba Mogie','Lalo Dyball','Alfons Massingberd','Benedick Hanratty','Ginni Trewett',
        'Luther Roydon','Rainer Ayllett','Shantee Ethridge','Charline Itzcovichch','Mirabel Adlard','Kevina Mailey','Deeyn Keenor','Ynez McMenamin',
        'Llewellyn Emlyn','Yanaton Greggersen','Maura Thunnercliffe','Farris Laville','Brande Tash','Sheilakathryn Lownie','Morena Ortsmann','Skipton Crockford',
        'Chelsey Blackborow','Kyle Birch','Haily Schanke','Rabbi Crepel','Danni Gudyer','Molli Sworne','Hermie Attwater','Antonella Danet','Clarette Halse',
        'Onfroi Ramsted','Son Marder','Amata Saville','Sula OSheeryne','Julianna Lorenzin','Antonella Forton','Nicolai McTrusty','Noell Jacobssen',
        'Gilemette Maylor','Sisile Teggart','Polly Quaintance',"Conrad Spilling","Julia Stopp","Inez Greeve","Lissa Snodden","Seymour Shegog","Molli Jakoubec",
        "Joelie Carruth","Barrie Zambon","Vasili Zahor","Nadeen McInility","Debi Adolfson","Tessa Joyson","Ryann Ludron","Keith Warricker","Daniele Hanlon",
        "Pyotr Paxman","Gerda Traher","Arvy Nathan","Whit Zute","Marcy Levicount","Vivyanne Pylkynyton","Port Culy","Nikolos Stonebridge","Erl Gutsell",
        "Chiquita Blaza","Pyotr Epsly","Damaris Devorill","Bob Emmet","Cathie O'Growgane","Siffre Fells","Dud Edgeworth","Alidia Korous","Arda Wasylkiewicz",
        "Gus Ashbridge","Lorie Rockcliffe","Carney O'Bradain","Corine Kemme","Zed Gavey","Mattie Brookwood","Linc de la Tremoille","Ardys Eliyahu",
        "Adella Crate","Herbie Sawrey","Olga Saberton","Ivette Dorant","Minnaminnie Iori","Christie Kilbey","Collie Wagenen","Jasper Seamer","Ashli M'cowis"]


        #list of patients on each hospital for a week
        # x of patients at index n in patientData means that the hospital at index n in hospitalData has x patients 
        self.patientData = patientData
        #list of hospitals
        
        self.hospitalData = ["Wuhan Jianghan Maternity and Child Health Care Center","Zhongnan Hospital of Wuhan University","Wuhan Central Hospital","Renmin Hospital of Wuhan University"
        ,"Wuhan No.1 Hospital","Western and Traditional Chinese Medicine Combined Hospital of Wuhan Xinzhou District","Wuhan Jiangxia District Traditional Chinese & Western Medicine Hospital"
        ,"Hanxi Branch of Wuhan Western and Traditional Chinese Medicine Combined Hospital", "Wuhan Hospital of Combined West and Traditional Chinese Medicine",
        "Wuhan University Stomatological Hospital","Wuhan Shunyuan Hospital of Traditional Chinese Medicine","Wuhan Brain Hospital"]
        
    # Getter
    
    def getNurseData(self):
        return self.nurseData

    def getHospitalData(self):
        return self.hospitalData

    def getPatientData(self):
        return self.patientData

    
        
# this class will contain the name and the shift for the nurse for a week
#parameter: String name
#length of shift is 14(2 shifts in 7 days = 14)
# 1 in shift means that the nurse is working and 0 means that the nurse is not working on a particular shift
# even index in shift equals to morning shift & odd index means night shift

class Nurse:

    def __init__(self,name):
        self.name = name
        self.shift = [] #max 14

    #setter

    def setName(self,name):
        self.name = name


    #getter

    def getName(self):    
        return self.name

    def getShift(self):
        return self.shift

    #add shift to the nurse
    #randomize the time when the nurse work(0 = doesn't work & 1 = work)
    def addShift(self):
        for i in range(14): #total number shifts in a week
            x = self.getShift()
            x.append(random.randint(0,1)) 

        return self

    def __str__(self):
        return self.name


class Genetic:
    def __init__(self,patientData):
        self.patientData = patientData

    def evolve(self,population):
        
        evPop = Population(0,self.patientData)
        evPopSchedule = evPop.getSchedule()
        for i in range(len(population.getSchedule())):
            newSchedule_1 = self.selection(population,).getSchedule()[0]
            newSchedule_2 = self.selection(population).getSchedule()[0]
            

            evPopSchedule.append(self.mutation(self.crossover(newSchedule_1, newSchedule_2)))
        evPop.getSchedule().sort(key = lambda x:x.getFitnessVal(), reverse = True)
        return evPop
        

    def selection(self,pop):
        pop = pop
        tournament_pop = Population(0,self.patientData)
        randomNum = 0

        i = 0
        while i < TOURNAMENT_SIZE:
            randomNum = random.randrange(0,pop.getSize()-1)
            selectSchedule = pop.getSchedule()[randomNum]
            tournament_pop.getSchedule().append(selectSchedule)
            i += 1
                
        
        tournament_pop.getSchedule().sort(key = lambda x:x.getFitnessVal(), reverse = True)
       
        return tournament_pop

    def crossover(self, schedule1, schedule2):
        offspringSchedule = Schedule(Data(self.patientData)).mulai()
        offspringSchedule2 = Schedule(Data(self.patientData)).mulai()

        for j in range(len(offspringSchedule.getHospitalList())):
            
            for k in range (len(offspringSchedule.getHospitalList()[j].getNurseList())):
                for l in range(len(offspringSchedule.getHospitalList()[j].getNurseList()[k].getShift())//2):
                    offspringSchedule.getHospitalList()[j].getNurseList()[k].getShift()[l*2] = schedule1.getHospitalList()[j].getNurseList()[k].getShift()[l*2]
                    offspringSchedule.getHospitalList()[j].getNurseList()[k].getShift()[l*2+1] = schedule2.getHospitalList()[j].getNurseList()[k].getShift()[l*2+1]

        for j in range(len(offspringSchedule2.getHospitalList())):
            for k in range (len(offspringSchedule2.getHospitalList()[j].getNurseList())):
                for l in range(len(offspringSchedule2.getHospitalList()[j].getNurseList()[k].getShift())//2):
                    offspringSchedule2.getHospitalList()[j].getNurseList()[k].getShift()[l*2] = schedule2.getHospitalList()[j].getNurseList()[k].getShift()[l*2]
                    offspringSchedule2.getHospitalList()[j].getNurseList()[k].getShift()[l*2+1] = schedule1.getHospitalList()[j].getNurseList()[k].getShift()[l*2+1]
        
        offspringSchedule.updateFitnessValue()
        offspringSchedule2.updateFitnessValue()

        if (offspringSchedule.getFitnessVal() > offspringSchedule2.getFitnessVal()):
            return offspringSchedule
        elif (offspringSchedule.getFitnessVal() < offspringSchedule2.getFitnessVal()):
            return offspringSchedule2
        else:
            if (random.random() > 0.5):
                return offspringSchedule
            else:
                return offspringSchedule2

    def mutation(self,schedule):
        
        rnd = random.random()
        if (rnd < 0.1):
            for i in schedule.getHospitalList():
                for j in i.getNurseList():
                    if (random.random() < 0.1):
                        for k in range(7):
                            if (j.getShift()[2*k+1] == 1 and j.getShift()[2*k] == 1):
                                if random.random() >= 0.5:
                                    j.getShift()[2*k+1] = 0
                                else:
                                    j.getShift()[2*k] = 0
        return schedule


   
#Hospital class contains the list of nurses, number of patients and hospital name
#parameter: String name, int patientNumber
class Hospital:
    def __init__(self, name, patientNumber):
        self.name = name
        self.patientNumber = patientNumber
        self.nurseList = []

    #getter

    def getName(self):
        return self.name

    def getPatientNumber(self):
        return self.patientNumber

    def getNurseList(self):
        return self.nurseList
        
    #add nurse to nurseList
    #parameter: Nurse newNurse
    def addNurse(self, newNurse):
        self.nurseList.append(newNurse)
    

#population class contains list of schedules(indviduals), the class data, and size of population 
#paramter: int size

class Population:
    
    def __init__(self,size,patientData):
        self.size = size
        self.schedule = []
        self.data = Data(patientData)

        #this block off code will initialize the creation of schedules(individuals) with randomized data
        for i in range(size):
            schedule = Schedule(Data(patientData))
            self.schedule.append(schedule.mulai().updateFitnessValue())
            
            

    #getter
    
    def getSchedule(self):
        return self.schedule
    def getData(self):
        return self.data
    def getSize(self):
        return len(self.schedule)
    

#schedule class contains list of nurse, list of hospital, fitness value
#parameter Data data

class Schedule:    
    def __init__(self,data):
        self.nurseList = []
        self.hospitalList = []
        self.fitnessVal = 0
        self.constraintSuccessive = 0
        self.constraintNurse = 0
        self.constraintEfficiency = 0
        self.givenData = data
        self.choosen = False


    #Check the fitness of the schedule(individual)
    #check if the there consecutive shifts(consecutive 1s) on each assigned nurses
    #if there are consecutive shifts then increment constraintsViolated by one
    #more constraints will be checked in the future

    def checkFitnessVal(self):
        weightA = 2
        weightB = 1
        weightC = 1
        denum = (self.constraintEfficiency*weightA) + (self.constraintNurse*weightB) + (self.constraintSuccessive*weightC) + 1
        maxVal = 1
        minVal = 1/(weightA + weightB + weightC + 1)
        tempFitnessVal = 1 / denum
        self.fitnessVal = (tempFitnessVal - minVal) / (maxVal-minVal)

        return self


    def constraintSuccessiveShift(self):
        constraintSucc = 0
        MAXSUCCESSIVESHIFT = 13
        constraintMax = 0

        for i in (self.hospitalList):
            constraintMax += len(i.getNurseList()) * MAXSUCCESSIVESHIFT
            for j in (i.nurseList):
                curShift=j.getShift()
                for k in range(len(curShift)):
                    if ((len(curShift)-1)!=k):
                        if (curShift[k]==1 and curShift[k+1]==1 ):
                            constraintSucc += 1
            
        self.constraintSuccessive += constraintSucc / constraintMax
        return self

    def constraintSameShiftDiffHospital(self):
        constraintSame = 0
        constraintMax = 0
        totalNurse = 0
        MAXSAME = 14
        nurseChecked = []
        for i in range (len(self.hospitalList)):
            totalNurse += (len(self.hospitalList[i].getNurseList()))
            for j in (self.hospitalList[i].nurseList):
                if (j.getName() not in nurseChecked):
                    nurseChecked.append(j.getName())
                    for k in range(i+1,len(self.hospitalList)):
                        for l in (self.hospitalList[k].nurseList):
                            if j.getName() == l.getName():
                                currentShift = j.getShift()
                                comparedShift = l.getShift()
                                for m in range(len(j.getShift())):
                                    if currentShift[m] == comparedShift[m] == 1:
                                        constraintSame += 1
        constraintMax = ((totalNurse - 1) * MAXSAME) 
        self.constraintHospital += (constraintSame / constraintMax)
        return self

    def constraintNurseExpected(self):
        constraintMax = 0
        totalDifference = 0
        for i in range(len(self.hospitalList)):
            constraintMax += (self.hospitalList[i].getPatientNumber() /6)
            totalDifference += abs((self.hospitalList[i].getPatientNumber() /6) - len(self.hospitalList[i].nurseList))
        self.constraintNurse += totalDifference / constraintMax
        return self
   
    def constraintNurseEfficiency(self):
        nurseAvailable = [0,0,0,0,0,0,0,0,0,0,0,0,0,0] #number of nurse in a hospital
        constraintMax = 0
        totalDifference = 0
        for i in range (len(self.hospitalList)):
            temp = 0
            difference = 0
            nurseExpected = []
            nurseAvailable = [0,0,0,0,0,0,0,0,0,0,0,0,0,0]
            expectedNurseMorning = math.ceil(math.ceil(self.hospitalList[i].getPatientNumber()/2)/6)
            expectedNurseNight = math.ceil(math.floor(self.hospitalList[i].getPatientNumber()/2)/6)

            constraintMax += ((expectedNurseMorning * 7 ) + (expectedNurseNight * 7))

            for j in range(7):
                nurseExpected.append(expectedNurseMorning)
                nurseExpected.append(expectedNurseNight)


            for j in (self.hospitalList[i].nurseList):
                tempShift = j.getShift()
                for k in range(14):
                    if tempShift[k] == 1:
                        temp = nurseAvailable[k] + 1
                        nurseAvailable.pop(k)
                        nurseAvailable.insert(k, temp)

            for j in range(14):
               difference +=  abs(nurseExpected[j] - nurseAvailable[j])
        totalDifference += difference
        self.constraintEfficiency += (totalDifference / constraintMax)

        return self

    
        
    #generate data for this class
    #this method will create hospitals based on data and put nurses in that hospital
    #constraints: two nurses with the same name can't be put twice or more in the same hospital. example: Hospital A: [Nurse1, Nurse1, Nurse2]
    #this method will check the name of the nurse before putting it in the list of nurse of the hospital

    def mulai(self):
        totalExpectedNurse = 0
        totalNurse = len(self.givenData.getNurseData())
        for i in range(len( self.givenData.getHospitalData())):
            self.hospitalList.append(Hospital( self.givenData.getHospitalData()[i], self.givenData.getPatientData()[i]))
            totalExpectedNurse += math.ceil( self.givenData.getPatientData()[i]/6)
            

        
        for i in self.getHospitalList():
            expectedNurse = math.ceil(i.getPatientNumber()/6)
            ratio = expectedNurse / totalExpectedNurse
            result = ratio * totalNurse
            
            if(result > expectedNurse):
                for j in range(expectedNurse):
                    nurseNameindex = random.randint(0,len( self.givenData.getNurseData())-1)
                    i.getNurseList().append(Nurse(self.givenData.getNurseData()[nurseNameindex]).addShift())
                    self.givenData.getNurseData().pop(nurseNameindex)
                      
            else:
                for k in range(round(result)):
                    nurseNameindex = random.randint(0,len( self.givenData.getNurseData())-1)
                    i.getNurseList().append(Nurse( self.givenData.getNurseData()[nurseNameindex]).addShift())
                    self.givenData.getNurseData().pop(nurseNameindex)
                    
             
        return self
    #getter

   

    def getFitnessVal(self):
        return self.fitnessVal

    def getHospitalList(self):
        return self.hospitalList

    def updateFitnessValue(self):
        self.fitnessVal = 0

        self.constraintEfficiency = 0
        self.constraintNurse = 0
        self.constraintSuccessive = 0

        self.constraintSuccessiveShift()
        self.constraintNurseExpected()
        self.constraintNurseEfficiency()
        
        self.checkFitnessVal()

        return self

    

initData = Data([])

def main(patientData):
    global initData
    #printing purposes
    initData = Data(patientData)
    newPop = Population(POPULATION_SIZE,patientData)
    counter = 1
    
    newPop.getSchedule().sort(key = lambda y:y.getFitnessVal(), reverse = True)

    first = newPop.getSchedule()[0].getFitnessVal()
    

    
    geneticAlgorithm = Genetic(patientData)
    
    
    i = 1
    while(i < 40):
        newPop = geneticAlgorithm.evolve(newPop)
        i += 1
    print("Highest fitness value for first population: " + str(first))
    print("Highest fitness value for latest generation: " + str(newPop.getSchedule()[0].getFitnessVal()))

    
    bestInd= newPop.getSchedule()[0]
    workbook= xlsxwriter.Workbook('./Output.xlsx') 
    worksheet= workbook.add_worksheet() 
    cell_format= workbook.add_format()
    cell_format.set_align('center')
    

    row= 0
    for k in range(0,len(bestInd.getHospitalList())):
        
        worksheet.write(row,0,bestInd.getHospitalList()[k].getName(),cell_format)
        worksheet.write(row,1,'Patient Number: '+str(bestInd.getHospitalList()[k].getPatientNumber()))
        worksheet.write(row,2,'Number of Nurses:' +str(len(bestInd.getHospitalList()[k].getNurseList())))
        
        worksheet.merge_range(row+1,1,row+1,14,'DAY',cell_format)
        worksheet.merge_range(row+2,1,row+2,2,'Monday')
        worksheet.merge_range(row+2,3,row+2,4,'Tuesday')
        worksheet.merge_range(row+2,5,row+2,6,'Wednesday')
        worksheet.merge_range(row+2,7,row+2,8,'Thursday')
        worksheet.merge_range(row+2,9,row+2,10,'Friday')
        worksheet.merge_range(row+2,11,row+2,12,'Saturday')
        worksheet.merge_range(row+2,13,row+2,14,'Sunday')
        
        worksheet.write(row+3,0,'Nurse Name:')

        for i in range (1,8):
            worksheet.write(row+3,(2*i)-1,'Morning')
            worksheet.write(row+3,(2*i),'Night')
        
        for i in range(0,len(bestInd.getHospitalList()[k].getNurseList())):
            worksheet.write(row+4+i,0,bestInd.getHospitalList()[k].getNurseList()[i].getName())
            
            for j in range(0,7):
                worksheet.write(row+4+i,(2*j)+1,bestInd.getHospitalList()[k].getNurseList()[i].getShift()[2*j])
                worksheet.write(row+4+i,(2*j)+2,bestInd.getHospitalList()[k].getNurseList()[i].getShift()[(2*j)+1])

        worksheet.write(row+4+len(bestInd.getHospitalList()[k].getNurseList()),0,'Total Nurse:')
        
        for i in range(0,14):
            count =0
            for j in range(0,len(bestInd.getHospitalList()[k].getNurseList())):

                if(bestInd.getHospitalList()[k].getNurseList()[j].getShift()[i]==1):
                    count+=1
            worksheet.write(row+4+len(bestInd.getHospitalList()[k].getNurseList()),1+i,count)
        row += (8+len(bestInd.getHospitalList()[k].getNurseList()))        

    workbook.close() 

    


if __name__ == "__main__":
    main()
            
            
